var personaArr = [
    {
        "personaId": 123,
        "name": "Jhon",
        "city": "Melburne",
        "phoneNo": "1234567890"
    },
    {
        "personaId": 124,
        "name": "Amelia",
        "city": "Sydney",
        "phoneNo": "1234567890"
    },
    {
        "personaId": 125,
        "name": "Emily",
        "city": "Perth",
        "phoneNo": "1234567890"
    },
    {
        "personaId": 126,
        "name": "Abraham",
        "city": "Perth",
        "phoneNo": "1234567890"
    }
];

var tabla = document.getElementById('ContTba');
var contenidot = document.createElement('tbody');

personaArr.forEach(p => {
    var fila = document.createElement('tr');
    
    var td = document.createElement('td');
    td.innerText = p.personaId;
    fila.appendChild(td);

    td = document.createElement('td');
    td.innerText = p.name;
    fila.appendChild(td);

    td = document.createElement('td');
    td.innerText = p.city;
    fila.appendChild(td);

    td = document.createElement('td');
    td.innerText = p.phoneNo;
    fila.appendChild(td);
    
    
    contenidot.appendChild(fila);
    
});


tabla.appendChild(contenidot);
