//Pregunta 11
console.log("Pregunta 11:");
function casos (x){
    switch(x){
        case "a":
        case "b":
        case "c":
            console.log("Either a, b or c was selected.");
        break;
        case "d":
            console.log("Only d was selected.");
        break;
        default:
            console.log("No case was matched.");
        break;
     }

}
casos("c");


//Pregunta 12
console.log("Pregunta 12:");
var x1 = 5 + 7 ;
var x2 = 5 + "7" ;
var x3 = "5" + 7 ;
var x4 = 5 - 7 ;
var x5 = 5 - "7" ;
var x6 = "5" - 7 ;
var x7 = 5 - "x" ;
console.log(x1);
console.log(x2);
console.log(x3);
console.log(x4);
console.log(x5);
console.log(x6);
console.log(x7);
