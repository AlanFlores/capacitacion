//Pregunta 23
console.log("Pregunta 23:");

function arreglo1(){
    var a = [1, 2, 3, 8, 9, 10];
    return a.slice(0, 3).concat([4,5,6, 7], a.slice(3,6));
}
function arreglo2(){
    var a = [1, 2, 3, 8, 9, 10];
    return a.splice(3, 0, ...[4, 5, 6, 7])
}
var ar1 = arreglo1();
var ar2 = arreglo2();

console.log(ar1);
console.log(ar2);

//Pregunta 24
console.log("Pregunta 24:")
function unir(){
        var array = ['a', 'b', 'c'];
        return (array.join('->') + ' '+ array.join('.') );
        }
var unirresultado = unir();
function separar(){
        return ('a.b.c'.split('.') + ' ' +'5.4.3.2.1'.split('.'));
        }
var separarresultado = separar();

function imprimir (a , b){
         console.log(a);
         console.log(b);
}

imprimir(unirresultado, separarresultado);



