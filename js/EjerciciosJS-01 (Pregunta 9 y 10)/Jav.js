//Pregunta 9
console.log("Pregunta 9:")
function animales (animal) {
    switch (animal){
        case 'Dog':
            console.log('I will not run since animal !== "Dog"');
        break;
        case 'Cat':
            console.log('I will not run since animal !== "Cat"');
        break;
        default:
            console.log('I will run since animal does not match other case');
        
    }
}
animales('Lion');

//Pregunta 10
console.log("Pregunta 10:")
function john(){
    return 'John';
}
function jacob(){
    return 'Jacob';
}
var j = john();
var j2= jacob();

function nombre(name) {
  switch(name){
    case john():
        console.log('I wiil run if name === "John"');
        break;
    case 'Ja' + 'ne':
        console.log('I wiil run if name === "Jane"');
        break;
    case john + ' ' + jacob() + ' Jingleheimer Schmidt':
        console.log('His name is equal to my name too!');
        break;
}
}

nombre(john());
