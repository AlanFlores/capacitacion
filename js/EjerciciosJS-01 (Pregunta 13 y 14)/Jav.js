//Pregunta 13
console.log("Pregunta 13:");
var a = 'hello' ||  ' ' ;
var b = '' || [] ;
var c = '' || undefined ;
var d = 1 || 5 ;
var e = 0 || {} ;
var f = 0 || '' || 5 ;
var g = '' || 'yay' || 'boo' ;
console.log(a);
console.log(b);
console.log(c);
console.log(d);
console.log(e);
console.log(f);
console.log(g);

//Pregunta 14
console.log("Pregunta 14:");
var a1 = 'hello' &&  '' ;
var b1 = '' && [] ;
var c1 = undefined && 0 ;
var d1 = 1 && 5 ;
var e1 = 0 && {} ;
var f1 = 'hi' && [] && 'done' ;
var g1 = 'bye' && undefined && 'adios' ;
console.log(a1);
console.log(b1);
console.log(c1);
console.log(d1);
console.log(e1);
console.log(f1);
console.log(g1);