//Pregunta 17
console.log("Pregunta 17:");
function continuar (){
    for (var i=0; i<3; i++){
        if (i === 1){
            continue;
        }
        console.log(i);
    }
}
continuar();

//Pregunta 18
console.log("Pregunta 18:");
function continuar2 (i){
    while ( i<3 ){
        if (i === 1){
            i = 2;
            continue;
        }
        console.log(i);
        i++;
    }
}
continuar2(0);

